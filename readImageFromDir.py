import cv2
import os

folderPath = input("Please Enter the full path of the image folder (example : c:/Users/$USER/Desktop/imageRestoration/training)")
if (os.path.exists(folderPath) and os.path.isdir(folderPath) ):
    folderPath.strip('\"')
else:
    print("Invalid Path or Path is not readable")
    exit(0)
         
valid_image_extensions = [".jpg", ".jpeg", ".png"]

def read_image_from_folder(folder):
    images = []
    for imageName in os.listdir(folder):
        extension  = os.path.splitext(imageName)[1]
        if extension.lower() not in valid_image_extensions:
            continue
        img = cv2.imread(os.path.join(folder,imageName))
        if img is not None:
            images.append(img);
    return images
imageList = read_image_from_folder(folderPath);

imageCount = 0
for image in imageList:
    # print(image)
    imageCount = imageCount + 1
    windowName  = "Image number {} Opened (Press Esc to for next Image)".format(imageCount)

    cv2.namedWindow(windowName, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(windowName, 600,600)
    cv2.imshow(windowName, image)
    cv2.waitKey()
    cv2.destroyAllWindows()
