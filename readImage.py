import cv2

img = cv2.imread('./python-logo.png')

cv2.imshow('Python Logo', img)

cv2.waitKey(0)
cv2.destroyAllWindows()