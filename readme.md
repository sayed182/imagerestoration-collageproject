1. Installing Python Open CV
   * python -m pip install opencv-python

2. Importing OpenCV
   * import open cv2

3. Reading Image
   * cv2.imread(PATH_TO_IMAGE,OPTIONAL_FLAG)

4. Displaying Image
   * cv2.imshow(IMAGE_STREAM)

